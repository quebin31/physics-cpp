cmake_minimum_required(VERSION 3.10)
project(Physics)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include_directories(include)
#include_directories(/home/kevin/Projects/C++/Gopt/include)
#include_directories(/home/kevin/Projects/C++/Flagger/include)

#add_executable(free_fallen test/FreeFallen.cpp)
#add_executable(parabolic_movement test/ParabolicMovement.cpp)
#add_executable(simple_harmonic_movement test/SimpleHarmonicMovement.cpp)
#add_executable(physics test/Physics.cpp)
add_executable(pm test/Pm.cpp)
