#ifndef PHYSICS_FREE_FALL_HPP
#define PHYSICS_FREE_FALL_HPP

#include <cmath>
#include <map>

#define FMT_HEADER_ONLY
#include <fmt/format.h>

namespace physics::real {

double free_fallen_analytic(double g, double m, double c, double t) {
  double v = (g*m)/c * (1 - std::exp((-c/m)*t));
  return v;
}

double free_fallen_numeric(double va, double g, double m, double c, double ta, double tb) {
  double vb = va + (g - c/m*va)*(tb - ta);
  return vb;
}

std::map<double, double> free_fallen_analytic_for(double g, double m, double c, double step, double tol = 0.1)  {
  std::map<double, double> values;
  double error = 100;
  double last_v = 0;
  double curr_v = 0;

  for (double t = 0; error >= tol; t += step) {
    curr_v = free_fallen_analytic(g, m, c, t);
    error  = t? curr_v - last_v : error;
    values.emplace(t, curr_v);
    last_v = curr_v;
  }

  return std::move(values);
}

std::map<double, double> free_fallen_numeric_for(double g, double m, double c, double step, double tol = 0.1) {
  std::map<double, double> values;
  double error = 100;
  double curr_v = 0;
  double new_v = 0;

  for (double t = 0; error >= tol; t += step) {
    new_v = free_fallen_numeric(curr_v, g, m, c, t, t + step);
    error = t? new_v - curr_v : error;
    values.emplace(t, curr_v);
    curr_v = new_v;
  }

  return std::move(values);
}


}

#endif
