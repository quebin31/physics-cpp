#include <FreeFallen.hpp>
#include <ParabolicMovement.hpp>

#include <Gopt.hpp>
#include <Flagger.hpp>

using namespace flagger::literals;

#define FMT_HEADER_ONLY
#include <fmt/format.h>

enum Flags {
  FREE_FALLEN        = 1_flag,
  PARABOLIC_MOVEMENT = 2_flag
};

int main(int argc, char** argv) {
  flagger::FlagController flag_controller;

  gopt::GetOptParser gopt_parser(argc, argv, {
    {'f', "free-fallen", "Free fallen case", no_argument, [&]{
      if (flag_controller.has_flag(PARABOLIC_MOVEMENT)) {
        fmt::print("Please choose only one test case");
        std::exit(EXIT_FAILURE);
      }

      flag_controller.add_flag(FREE_FALLEN);
    }},
    {'p', "parabolic-movement", "Parabolic movement case", no_argument, [&]{
      if (flag_controller.has_flag(FREE_FALLEN)) {
        fmt::print("Please choose only one test case");
        std::exit(EXIT_FAILURE);
      }

      flag_controller.add_flag(PARABOLIC_MOVEMENT);
    }}
  });

  gopt_parser.parse([&]{
    if (optind == 1) {
      fmt::print("Please choose at least one test case");
      std::exit(EXIT_FAILURE);
    }
  });

  if (flag_controller.has_flag(FREE_FALLEN)) {

  }

  else if (flag_controller.has_flag(PARABOLIC_MOVEMENT)) {

  }

  else {

  }
}
