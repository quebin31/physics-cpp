#include <ParabolicMovement.hpp> 

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

using namespace fmt;
using namespace physics::ideal;

int main() {
    auto data = parabolic_movement_ideal(56.5, 60, 9.8, 1);
    print("{:>10} {:>10} {:>10} {:>10} {:>10}\n", "t", "x", "y", "vx", "vy");
        
    for (auto& d: data)
        print("{:>10} {:>10} {:>10} {:>10} {:>10}\n", d.t, d.x, d.y, d.sx, d.sy);

}
