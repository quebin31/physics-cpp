#include <SimpleHarmonicMovement.hpp>

#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include <fmt/ostream.h>

#include <iostream>
#include <fstream>

int main(int argc, char** argv) {
  {
    double angle, speed, length, gravity, step;
    unsigned long steps;
    std::string speed_fname, angle_fname;

    fmt::print("Pendulum case\n");
    fmt::print("angle, speed, length, gravity: ");
    std::cin >> angle >> speed >> length >> gravity;
    fmt::print("steps, step size: ");
    std::cin >> steps >> step;
    fmt::print("name of speed and angle files: ");
    std::cin >> speed_fname >> angle_fname;

    auto data = physics::ideal::pendulum(speed, angle, length, gravity, 5000,0.001);

    std::ofstream speed_file(speed_fname);
    std::ofstream angle_file(angle_fname);

    if (!speed_file || !angle_file)
      return 1;

    for (auto &d : data) {
      fmt::print(speed_file, "{:.6f} {:.6f}\n", d.time, d.speed);
      fmt::print(angle_file, "{:.6f} {:.6f}\n", d.time, d.angle);
    }
  }

  {
    double mass, k, position, speed, step;
    unsigned long steps;
    std::string speed_fname, pos_fname;

    fmt::print("Spring case\n");
    fmt::print("mass, k, position, speed: ");
    std::cin >> mass >> k >> position >> speed;
    fmt::print("steps and step size: ");
    std::cin >> steps >> step;
    fmt::print("name of speed and position files: ");
    std::cin >> speed_fname >> pos_fname;

    auto data = physics::ideal::spring(mass, k, position, speed, steps, step);

    std::ofstream speed_file(speed_fname);
    std::ofstream posit_file(pos_fname);

    if (!speed_file || !posit_file)
      return 1;

    for (auto &d : data) {
      fmt::print(speed_file, "{:.6f} {:.6f}\n", d.time, d.speed);
      fmt::print(posit_file, "{:.6f} {:.6f}\n", d.time, d.position);
    }
  }
}
